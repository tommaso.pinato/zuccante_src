# es005_crud_hibernate

Tratto da un tutorial ufficiale [qui](https://spring.io/guides/gs/accessing-data-mysql/) usa i seguenti moduli
- **Spring Boot Dev Tools**
- **Spring Web**
- **Spring Data JPA: Hibernate e soci**
- **MariaDB Driver**
- **Mustache**
Dell'originale è presente il `UserRestController`

## creazione del DB su MariaDB

Qui sotto le istruzioni per MariaDB (MySQL)
```
CREATE DATABASE es005SB;

GRANT ALL ON es005SB.* to 'es005SB'@localhost IDENTIFIED BY 'zuccante@2021';

SHOW GRANTS FOR 'es005SB'@localhost;
```
si può quindi `mysql -u es005SB -p`.

## le impostazioni per il DB

In `application.properties` impostiamo driver e parametri per il DB
```
# DB
spring.datasource.url=jdbc:mariadb://localhost:3306/es005SB
spring.datasource.username=es005SB
spring.datasource.password=zuccante@2021
spring.datasource.driver-class-name=org.mariadb.jdbc.Driver
```

## repository

Come nel tutorial originale usiamo un `CrudRepository<T,ID>` [qui](https://docs.spring.io/spring-data/commons/docs/2.4.6/api/), la soluzione più minimale.

## controller

Sull'esempio originale`@ResponseBody` prepara il tipo di ritorno per un `body` con l'*header* opportuno, di tipo `json`, `@RequestParam` invece permette di recuperare
i *request param*, [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/) per le API, non `json`
```
curl localhost:8080/rest/user/add -d name=First -d email=someemail@someemailprovider.com
```
La classe `UserRestController` è un semplice REST `jason` (vedi es004). Veniamo al controller `UserController` da noi scritto, in particolare
```java
// GET
@GetMapping(path="/{id}")
public String getUser(Model model, @PathVariable int id) {
    User user = userRepository.findById(id).orElse(null);
    if(user != null) {
        model.addAttribute("id", user.getId());
        model.addAttribute("name", user.getName());
        model.addAttribute("email", user.getEmail());
    } else {
        model.addAttribute("id", "NULL");
        model.addAttribute("name", "NULL");
        model.addAttribute("email", "NULL");
    }
    return "user";
}
```
per recuperare il singolo utente usando `@PathVariable int id`. Ancora:
```java
// GET form for edit user
@GetMapping(path="/edit/{id}")
public String formEditUser(Model model, @PathVariable int id) {
    User user = userRepository.findById(id).orElse(null);
    if(user != null){
        model.addAttribute("id", user.getId());
        model.addAttribute("name", user.getName());
        model.addAttribute("email", user.getEmail());
    }
    return "editUser";
}
```
per il form di edit cui segue il POST
```java
 // POST edit (update)
    @Transactional
    @PostMapping(path="/edit")
    String editUser (Model model, @RequestParam int id, @RequestParam String name
            , @RequestParam String email) {
        User user = userRepository.findById(id).orElseThrow(
                ()->new IllegalStateException("user does not exist, id: " + id));
        user.setName(name);
        user.setEmail(email);
        // userRepository.save(user);
        return "redirect:/user/";
    }
```
Purtroppo *Spring* non ci permette di passa su POST la `@PathVariable`, optiamo quindi per passare `id` attraverso il FORM, sempre con tecniche HTML5 avremmo potuto usare un *hidden field*
```html
<input type="hidden" id="custId" name="custId" value="3487">
```
con `@Transactional` indichiamo, vedi [qui](https://docs.spring.io/spring-framework/docs/5.3.10-SNAPSHOT/javadoc-api/org/springframework/transaction/annotation/Transactional.html) indichiamo che
le operazioni sugli oggetti persistenti vanno effettuati come una **transazione**.
Non è necessario
```java
userRepository.save(user);
```
i *setter* già operano su di un'entità *persistent*.
