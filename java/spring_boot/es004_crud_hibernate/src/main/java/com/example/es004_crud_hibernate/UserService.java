package com.example.es004_crud_hibernate;

import com.example.es004_crud_hibernate.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private boolean existsById(long id) {
        return userRepository.existsById(id);
    }

    // GET
    public User findById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    // GET
    public List<User> findAll(int pageNumber, int rowPerPage) {
        List<User> users = new ArrayList<>();
        Pageable sortedByLastUpdateDesc = PageRequest.of(pageNumber -1, rowPerPage, Sort.by("id").ascending());
        userRepository.findAll(sortedByLastUpdateDesc).forEach(users::add);
        return users;
    }

    // POST
    public User save(User user) throws Exception {
        if (user.getName() == null) {
            throw new Exception("Name is required");
        }
        if (user.getEmail() == null) {
            throw new Exception("Email is required");
        }
        if (existsById(user.getId())) {
            throw new Exception("User with id: " + user.getId() + " already exists");
        }
        return userRepository.save(user);
    }

    // PUT
    public void update(User user) throws Exception {
        if (user.getName() == null) {
            throw new Exception("Name is required");
        }
        if (user.getEmail() == null) {
            throw new Exception("Email is required");
        }
        if (!existsById(user.getId())) {
            throw new Exception("Cannot find User with id: " + user.getId());
        }
        userRepository.save(user);
    }

    // DELETE
    public void deleteById(Long id) throws Exception {
        if (!existsById(id)) {
            throw new Exception("Cannot find User with id: " + id);
        }
        else {
            userRepository.deleteById(id);
        }
    }

    // GET
    public Long count() {
        return userRepository.count();
    }
}
