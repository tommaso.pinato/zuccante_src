# es010_rest_again

Il seguente esempio è tratto dall'ottimo tutorial [qui](https://www.youtube.com/watch?v=9SGDpanrc8U&t=19s).

## creazione progetto

- Spring Web
- Spring Data JPA
- Spring Boot DevTools
- MariaDB Driver

## DB e model

```
CREATE DATABASE es010SB;
GRANT ALL ON es010SB.* to 'es010SB'@localhost IDENTIFIED BY 'zuccante@2021';
```

->`@Id`: indica la chiave primaria  
->`@@GeneratedValue(strategy = GenerationType.IDENTITY)`: non facciamo uso di un *sequence generator* ma facciamo affidamento a `MariDB`
alternativamente, come nel video, potremmo usare un *generator*, [qui](https://docs.jboss.org/hibernate/orm/5.5/userguide/html_single/Hibernate_User_Guide.html#identifiers-generators-sequence) 
per i dettagli.  
-> `@Transient` si presta ottimamente per gli **attributi derivati** (vedi ER).
> The `@Transient` annotation is used to specify that a given entity attribute should not be persisted.

-> ricordiamo, onde non far arrabbiare Hibernate il *default contructor*

## L'idea

- ***controller*** per le API REST (json)
- ***model*** per gli oggetti persistenti, file `Student.java`
- ***service*** la *business logic*
- ***repository*** interfacciamento col DB (assieme a *model*)
- ***config*** configurazione iniziale mediante *bean*

## Service

Pensato per la *business logic* (vedi appunti sulle *annotation*), usiamo `IllegalStateException` in quanto è una `RuntimeException`
> RuntimeException is the superclass of those exceptions that can be thrown during the normal operation of the Java Virtual Machine.  
RuntimeException and its subclasses are unchecked exceptions. 
Unchecked exceptions do not need to be declared in a method or constructor's throws clause if they can be thrown by the execution of the method or constructor and propagate outside the method or constructor boundary.

Per l'aggiornamento usiamo l'*annotation* `@Transactional`, [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/transaction/annotation/Transactional.html) per
la documentazione), [qui](https://dzone.com/articles/how-does-spring-transactional) un articolo per approfondire: JPA non fornisce supporto per le transazioni, ci viene incontro Spring.

## Repository

`Student` e `Long` sono i *generic* per gli oggetti e la chiave;

## Controller
 Basato sulla *business logic* del *service* contiene il cuore del REST

### GET

Ci limitiamo ad eslencare tutti gli studenti.

### POST

```
curl -X POST -H "Content-Type: application/json" \
 -d '{"name":"Armando","email":"armando.deraza@gmail.com", "dob":"1971-03-17"}' \
 http://localhost:8080
```

### PUT

```
curl -X PUT -F 'name=Bepi2' http://localhost:8080/1
```
**NB** I *setter** aggiornano un'entità in modo persistente! Non è necessario richiamare `save()`.


### DELETE

```
 curl -X DELETE http://localhost:8080/1
```

## Configurazione

Abbiamo creato un ulteriore file di configurazione per popolare il DB nel momento in cui lanciamo l'applicazione: `StudentConfig`.
`CommandLineRunner`, [qui](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/CommandLineRunner.html) per la documentazione,
permette di ...
> Interface used to indicate that a bean should run when it is contained within a SpringApplication.

