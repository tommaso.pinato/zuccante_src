package com.example.es006_rest_raw_sql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/person")
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    // GET
    @GetMapping(path="/all")
    public @ResponseBody List<Person> getAllPersons() {
        return personRepository.findAll();
    }

    // GET
    @GetMapping(path="/{firstName}")
    public @ResponseBody List<Person> getPersonByFirstName(@PathVariable String firstName) {
        return personRepository.findByFirstName(firstName);
    }

    // POST
    @PostMapping(path="/add")
    public String addNewUser (@RequestBody Person person) {
        personRepository.insertWithQuery(person);
        return "{\"message\": \"saved\"}";
    }






}
