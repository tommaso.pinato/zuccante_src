package com.example.es007_form_submission;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MyController {

    @GetMapping("/formgreeting")
    public String greetingForm(Model model) {
        model.addAttribute("greeting", new Greeting());
        return "formgreeting";
    }

    @PostMapping("/formgreeting")
    public String greetingSubmit(Model model, @ModelAttribute("greeting") Greeting greeting) {
        model.addAttribute("id", greeting.getId());
        model.addAttribute("content", greeting.getContent());
        return "showgreeting";
    }

    @GetMapping("/formparam")
    public String getFormParam(Model model) {
        return "formparam";
    }

    /*
    @GetMapping("/showParam")
    public String showParam(Model model) {
        return "showParam";
    }
    */

    @PostMapping("/formparam")
    public String postFormParam(Model model, @RequestParam("param") String param) {
        model.addAttribute("param", param);
        return "showparam";
    }


}
