# es003_template

Qui vediamo l'uso dei template per creare pagine dinamiche; non abbiamo articolato l'applicazione come in precedenza usando un *component* (lì era un *service*)

## mustache il template

Prende il nome da `{{..}}`, per un manuale veloce [qui](https://mustache.github.io/mustache.5.html). Qui di seguito diamo alcuni esempi d'uso
```
{{name}}
```
visualizza il contenuto di `name` (leggendo pure gli eventuali TAG html). 
```
Shown
{{#person}}
 Never shown!
{{/person}}
```
se `person` è `false` o **lista vuota**
```
Shown
```
Per **liste non vuote**, come nel nostro caso,
```
{{#repo}}
  <b>{{name}}</b>
{{/repo}}
```
con
```
{
"repo": [
    { "name": "resque" },
    { "name": "hub" },
    { "name": "rip" }
  ]
}
```
abbiamo
```
<b>resque</b>
<b>hub</b>
<b>rip</b>
```
Possiamo nidificare template con `{{> box}}`: i *partial*, così si chiamano, sono applicati al momento del *rendering* pertanto è possibile usarli anche in modo ricorsivo.
Nel nostro esempio abbiamo per il template `book.mustache`
```
{{>header}}
<h1>Mustache with Spring Boot</h1>

<p>Un primo esempio con <i>Mustache</i></p>
<h2>Book List </h2>
{{#bookList}}
    <b> Book ID: </b>{{bookid}}
    <b> ISBN Number: </b>{{isbn}}
    <b> Book Title: </b>{{bookTitle}}
    <b> Author: </b>{{author}}
    <b> Price: </b>{{price}}
    <br>
{{/bookList}}
{{>footer}}
```

## il model

La classe `Book` l'abbiamo creata con i costruttori; essenziale è quello senza parametri, i *getter* ed i *setter* sono stati aggiunti con **IDEA**.


## i controller

Soffermiamoci in particolare su
```java
@RestController
public class BookController {

    @GetMapping("/books")
    public ModelAndView getProducts(Map<String, Object> model){

        List<Book> bookList = IntStream.range(0,7)
                .mapToObj(this::getBook)
                .collect(Collectors.toList());

        model.put("bookList", bookList);
        return new ModelAndView("book", model);
    }

    // create a book
    private Book getBook(int i){
        return new Book(i,
                "ISBN Number -" + i,
                "Book Name " + i,
                "Author " + i,
                (double) (100 * i));
    }
}
```
Qui usiamo `ModelAndView`, doc [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/servlet/ModelAndView.html).

Dalla documentazione
> *Holder for both Model and View in the web MVC framework. Note that these are entirely distinct. This class merely holds both to make it possible for a controller to return both model and view in a single return value. *

Il costruttore ([qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/servlet/ModelAndView.html#ModelAndView-java.lang.String-))
```java
public ModelAndView(String viewName,
            @Nullable
            Map<String,?> model)
```
crea un `ModelView` associando un *model*, `viewName` indica il *template* da utilizzare; *model* è una mappa fra i nomi delle variabili nel template e gli oggetti ad esse associati.