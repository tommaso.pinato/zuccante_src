package com.example.es013_authentication_jpa;

import com.example.es013_authentication_jpa.User;
import com.example.es013_authentication_jpa.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    // register a new user
    public void addNewUser(User user) {
        Optional<User> optionalStudent = userRepository.findByUserName(user.getUserName());
        if(optionalStudent.isPresent()){
            throw new IllegalStateException("user with " + user.getUserName() + "is present");
        }
        userRepository.save(new User(user.getUserName(), passwordEncoder.encode(user.getPassword()))
        );
    }

    public void deleteUser(Integer id) {
        if(!userRepository.existsById(id))
            throw new IllegalStateException("user does not exist, id: " + id);
        userRepository.deleteById(id);
    }

    @Transactional
    public void updateStudent(Integer id, String userName, String password) {
        User user = userRepository.findById(id).orElseThrow(
                ()->new IllegalStateException("user does not exist, id: " + id)
        );
        if(userName != null && userName.length() > 0 && !Objects.equals(user.getUserName(), userName))
            user.setUserName(userName);
        if(password != null && password.length() > 0 && !Objects.equals(user.getPassword(), password)){
            user.setPassword(passwordEncoder.encode(password));
        }
    }
}
