package com.example.es002_restfull;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {

    private final List<Student> studentRecords;

    private StudentService() {
        studentRecords = new ArrayList<>();
    }

    // GET
    public List<Student> getStudentRecords() {
        return studentRecords;
    }

    // GET
    public Student getStudent(String registrationNumber) {
        for (Student stdn : studentRecords) {
            if (stdn.getRegistrationNumber().equals(registrationNumber)) {
                return stdn;
            }
        }
        return new Student();
    }

    // POST
    public void add(Student std) {
        studentRecords.add(std);
    }

    // PUT
    public String upDateStudent(Student std) {
        for(int i=0; i<studentRecords.size(); i++) {
            Student stdn = studentRecords.get(i);
            if(stdn.getRegistrationNumber().equals(std.getRegistrationNumber())) {
                studentRecords.set(i, std); // update the new record
                return "Update successful:" + stdn.getRegistrationNumber();
            }
        }
        return "*** Update un-successful";
    }

    // DELETE
    public String deleteStudent(String registrationNumber) {

        for(int i=0; i<studentRecords.size(); i++) {
            Student stdn = studentRecords.get(i);
            if(stdn.getRegistrationNumber().equals(registrationNumber)){
                studentRecords.remove(i);//update the new record
                return "Delete successful" + stdn.getRegistrationNumber();
            }
        }
        return "*** Delete un-successful";
    }
}
