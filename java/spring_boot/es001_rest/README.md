# es001_rest

Il primo esempio di REST; per lanciarlo usiamo
```
nodemon --exec "./gradlew bootRun
```
ottima la documentazione ufficiale, [qui](https://spring.io/guides/gs/rest-service/), da cui è tratto parte dell'esemipo!