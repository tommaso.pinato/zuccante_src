# es008_relationships

Tratto da [qui](https://www.baeldung.com/spring-data-rest-relationships) usa i seguenti moduli
- **Spring Boot Dev Tools**
- **Spring Web**
- **Spring Data JPA: Hibernate e soci**
- **REST repositories** *Exposing Spring Data repositories over REST via Spring Data REST.*
- **MariaDB Driver**
  Niente template! Un'applicazione REST pura.

## creazione del DB su MariaDB

Qui sotto le istruzioni per MariaDB (MySQL)
```
CREATE DATABASE es008SB;

GRANT ALL ON es008SB.* to 'es008SB'@localhost IDENTIFIED BY 'zuccante@2021';

SHOW GRANTS FOR 'es008SB'@localhost;
```
si può quindi `mysql -u es005SB -p`.

## le impostazioni per il DB

In `application.properties` impostiamo driver, parametri per il DB e per Hibernate
```
# DB
spring.datasource.url=jdbc:mariadb://localhost:3306/es008SB
spring.datasource.username=es008SB
spring.datasource.password=zuccante@2021
spring.datasource.driver-class-name=org.mariadb.jdbc.Driver

# hibernate
spring.jpa.hibernate.ddl-auto=create-drop
spring.jpa.hibernate.use-new-id-generator-mappings=false
```
Lanciare per due volte il server!

## One To One

Sulla documentazione di Hibrnate [qui](https://docs.jboss.org/hibernate/orm/5.4/userguide/html_single/Hibernate_User_Guide.html#associations-one-to-one): abbiamo la possibilità di definirla in modo
**unidirezionale** e **bidirezionale**, qui usiamo la prima possibilità. Ad esempio
```java
@OneToOne
@JoinColumn(name = "details_id")
private PhoneDetails details;
```
con `@JoinColumn` possiamo (opzionele) definire il nome della colonna cui punta la chiave esterna (che è anche primaria) nell'*entity* *target* (tale annotazione la riprendiamo nel dettaglio
in `@ManyToMany`).  Per la bidirezionalità useremo sul target *@MappedBy*, ad esempio
``` java
@OneToOne(
    mappedBy = "phone",
    ...
)
```
Per l'*annotation* `@RestResource` (opzionale) [qui](https://docs.spring.io/spring-data/rest/docs/3.4.7/api/), 
essa si rivela utile nel caso, fra due `Entity` si volessero realizzare più associazione, ad esempio un secondo indirizzo (1:1 su di un secondo indirizzo),
noi abbiamo messo la non necessaria
```java
@RestResource(path = "libraryAddress", rel="address")
```
Mandato in esecuzione vediamo dentro MariaDB il risultato
```
MariaDB [es008SB]> describe address;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | bigint(20)   | NO   | PRI | NULL    | auto_increment |
| location | varchar(255) | NO   |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
2 rows in set (0.002 sec)

MariaDB [es008SB]> describe library;
+------------+--------------+------+-----+---------+----------------+
| Field      | Type         | Null | Key | Default | Extra          |
+------------+--------------+------+-----+---------+----------------+
| id         | bigint(20)   | NO   | PRI | NULL    | auto_increment |
| name       | varchar(255) | YES  |     | NULL    |                |
| address_id | bigint(20)   | YES  | MUL | NULL    |                |
+------------+--------------+------+-----+---------+----------------+
3 rows in set (0.002 sec)
```
con `SHOW CREATE TABLE library` possiamo vedere la chiave esterna! Passiamo al primo testi
```
curl -i -X POST -H "Content-Type:application/json" -d '{"name":"My Library"}' http://localhost:8080/libraries
HTTP/1.1 201 
Vary: Origin
Vary: Access-Control-Request-Method
Vary: Access-Control-Request-Headers
Location: http://localhost:8080/libraries/1
Content-Type: application/hal+json
Transfer-Encoding: chunked
Date: Tue, 06 Apr 2021 15:22:21 GMT

{
  "name" : "My Library",
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/libraries/1"
    },
    "library" : {
      "href" : "http://localhost:8080/libraries/1"
    },
    "address" : {
      "href" : "http://localhost:8080/libraries/1/libraryAddress"
    }
  }

```
infatti
```
MariaDB [es008SB]> SELECT * FROM library;
+----+------------+------------+
| id | name       | address_id |
+----+------------+------------+
|  1 | My Library |       NULL |
+----+------------+------------+
1 row in set (0.001 sec)
```
per sperimentare a conferma
```
http://localhost:8080/libraries/1
```
anche
```
http://localhost:8080/libraries
```
Aggiungiamo un indirizzo
```
curl -i -X POST -H "Content-Type:application/json" -d '{"location":"Main Street nr 5"}' http://localhost:8080/addresses
```
e a conferma
```
curl http://localhost:8080/addresses
```


e quindi creiamo l'associazione con un `PUT` (aggiorniamo `library`)
```
curl -i -X PUT -d "http://localhost:8080/addresses/1" -H "Content-Type:text/uri-list" http://localhost:8080/libraries/1/libraryAddress
```
Usiamo l'*header* "Content-Type:text/uri-list" in modo da poter inviare un *url*, a conferma
```
MariaDB [es008SB]> SELECT * FROM library;
+----+------------+------------+
| id | name       | address_id |
+----+------------+------------+
|  1 | My Library |          1 |
+----+------------+------------+
1 row in set (0.001 sec)
```
ma anche
```
curl http://localhost:8080/libraries/1/libraryAddress
{
  "location" : "Main Street nr 5",
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/addresses/1"
    },
    "address" : {
      "href" : "http://localhost:8080/addresses/1"
    },
    "library" : {
      "href" : "http://localhost:8080/addresses/1/library"
    }
  }
}
```
ovvero
```
curl http://localhost:8080/libraries/1
{
  "name" : "My Library",
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/libraries/1"
    },
    "library" : {
      "href" : "http://localhost:8080/libraries/1"
    },
    "address" : {
      "href" : "http://localhost:8080/libraries/1/libraryAddress"
    }
  }
}
```

## One To Many and Mant to One

Introduciamo l'*entuty* `Book`, sulla documentazione di Hibernate troviamo [qui](https://docs.jboss.org/hibernate/orm/5.4/userguide/html_single/Hibernate_User_Guide.html#associations-one-to-many) l'argomento.
Una tale associazione viene definita una *parent association* nel senso che la si guarda dal lato *One*, lavorando in modo **bidirezionale** si usa l'annotation
`@ManyToOne`, quindi abbiamo
```java
@OneToMany(mappedBy = "library")
private List<Book> books;
```
Lato `Library` - *One* - ove `library` (vedi qui sotto) è campo di `Book` e lato `Book` - *Many* - tutto come sopra
```java
@ManyToOne
@JoinColumn(name="library_id")
private Library library;
```
Partiamo da un DB vuoto (vedi anche impostazioni dell'applicazione) ed introduciamo e di nuovo possiamo verificare
```
SHOW CREATE TABLE book;
```
di nuovo niente `ON UPDATE` o `ON DELETE`, non ci preoccupiamo di problemi di **integrità** che possono sorgere! Introduziamo il nostro libro
```
curl -i -X POST -H "Content-Type:application/json" -d '{"name":"My Library"}' http://localhost:8080/libraries
```
nel `json` 
```
"library" : {
      "href" : "http://localhost:8080/books/1/library"
    }
```
ed infatti
```
curl http://localhost:8080/books/1/library
```
non ritorna nulla, aggiorniamo illegame
```
curl -i -X PUT -H "Content-Type:text/uri-list" -d "http://localhost:8080/libraries/1" http://localhost:8080/books/1/library
```
e la precedente ci dà un risultato. Aggiungiamo quindi un secondo libro
```
curl -i -X POST -d '{"title":"Book2"}' -H "Content-Type:application/json" http://localhost:8080/books
```
leghiamolo alla nostra libreria
```
curl-i -X PUT -H "Content-Type:text/uri-list" -d "http://localhost:8080/libraries/1" http://localhost:8080/books/2/library
```
e vediamo i linri nella libreria
```
curl http://localhost:8080/libraries/1/books
```
risultato
```
"_embedded" : {
    "books" : [ ... ]
    }
```

## MAny To Many

Questo è il caso più complesso in quanto, come ben sappiamo, dobbiamo aggiungere una tabella - *annotation* `@JoinTable` - di appoggio (con 2 chiavi esterne).
Nella documentazione ufficiale di Hibernate il link è [qui](https://docs.jboss.org/hibernate/orm/5.4/userguide/html_single/Hibernate_User_Guide.html#associations-many-to-many);
per le API [qui](https://javaee.github.io/javaee-spec/javadocs/javax/persistence/ManyToMany.html). Per prima cosa definiamo il nome della * join table*, ecco un esempio minimale
```java
@JoinTable(name="booksAuthors")
```
ad esso corrisponde
```java
@ManyToMany(mappedBy="phones")
```
Quindi ancora uan situazione asimmetrica (partenza -> arrivo) come in *One To One*. Passiamo al dettaglio dell'*annotation* `@JoinTable` ovvero alle API [qui](https://javaee.github.io/javaee-spec/javadocs/javax/persistence/JoinTable.html)
- `name`: il nome della tebella (opzionale)
- `joinColumn`: si riferisce all'*entity* di partenza (opzionale)
- `inverseJoinColumn`: si riferisce all'*entity* di arrivo (opzionale)
per ognuno abbiamo`@JoinColumn` [qui](https://javaee.github.io/javaee-spec/javadocs/javax/persistence/JoinColumn.html) per le API
- `name`: il nome della **chiave esterna**.
- `referenceColumn`: la colonna a cui si riferisce`
Nel nostro caso
```java
 @JoinTable(name = "book_author",
        joinColumns = @JoinColumn(name = "book_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "author_id", referencedColumnName = "id"))
```
Tornando a `#ManyToMany` (sopra il link alle API) 
- `cascade` definisce le operazioni che bisogna fare sul *target*, [qui](https://javaee.github.io/javaee-spec/javadocs/javax/persistence/CascadeType.html) le possibilità
- `ALL` riporta tutte le operazioni.
Via col test: aggiungiamo 2 autori
```
curl -i -X POST -H "Content-Type:application/json" -d '{"name":"author1"}' http://localhost:8080/authors
curl -i -X POST -H "Content-Type:application/json" -d '{"name":"author2"}' http://localhost:8080/authors
```
e 2 libri
```
curl -i -X POST -d '{"title":"Book1"}' -H "Content-Type:application/json" http://localhost:8080/books
curl -i -X POST -d '{"title":"Book2"}' -H "Content-Type:application/json" http://localhost:8080/books
```
Creiamo quindi l'associazione . Per prima cosa creiamo il file `uris.txt` grazie ad esso potremo mandare più `uri` separati da un *a capo*.
```
nano uri.txt
```
In esso mettiamo
```
http://localhost:8080/books/1
http://localhost:8080/books/2
```
`^O` e `^X`, quindi
```java
curl -i -X PUT -H "Content-Type:text/uri-list" --data-binary @uris.txt http://localhost:8080/authors/1/books
```
possiamo provare
```
curl http://localhost:8080/authors/1/books
```
per poi aver conferma in MariaDB
```
SELECT * FROM book_author;
+---------+-----------+
| book_id | author_id |
+---------+-----------+
|       1 |         1 |
|       1 |         2 |
+---------+-----------+
2 rows in set (0.001 sec)

```

