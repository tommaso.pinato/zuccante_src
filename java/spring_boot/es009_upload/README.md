# es009_upload

Questo esempio è tratto da uno dei piccoli tutorial ufficiali [qui](https://spring.io/guides/gs/uploading-files/),
sono stati usati i seguenti moduli
- **Spring Boot Dev Tools**
- **Spring Web**
- **mustache**

Altri tutorial simili in rete, come [questo](https://grokonez.com/spring-framework/spring-boot/multipartfile-create-spring-ajax-multipartfile-application-downloadupload-files-springboot-jquery-ajax-bootstrap).
che si trovano in rete semplificando. 

## il service

```java
public interface FileStorageService {

    public void init();

    public void save(MultipartFile file);

    public Resource load(String filename);

    public void deleteAll();

    public Stream<Path> loadAll();
}
```
definisce il protocollo del *service* che poi andiamo a definire nella classe `FileStorageServiceImpl`.
Per le APi di `@Service` [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/stereotype/Service.html) è un `@Component`, mediante l'*annotation* 
indichiamo una classe debutata in modo particolare alla ***business logic***; in essa facciamo uso di alcune classi Java per la gestione dei file
- `Path` è un'interfaaccia per gestire alberi di d*directory* e *file*. `Paths` come `Files` sono classi utility con metodi statici - *factory*.
- `MultipartFile` [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/multipart/MultipartFile.html) per le API, implementa `InputstreamResource` con `getName()` otteniamo il nome nel `form` e con `getOriginalName()` otteniamo il nome nel *file system*.
- `Resource` [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/core/io/Resource.html) per le API, anche questa come la precede estende `InpustreamResource` ed è un interfaccia.
- `UrlResource` [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/core/io/UrlResource.html) per le API, implementa l'interfaccia di cui sopra pensata per URL 
e la useremo per poter scaricare i file `Resource`

Interessante è il metodo
```java
@Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.upload, 1)
                    .filter(path -> !path.equals(this.upload))
                    .map(this.upload::relativize);
        } catch (IOException e) {
            throw new RuntimeException("Could not load the files!");
        }
    }
```
dove usiamo il NIO fornendo uno stream di PATH che useremo: nel *controller* verrà usato in mod interessante.

## controller

QUesto controller è pensato per una *qebapp*. SUbito all'inizio troviamo
```java
@Autowired
FileStorageService service;
```
Ci troviamo di fronte alla ***dependency injection***. Per approfondire partiamo da una modalità di disegno - talvolta lo si chiama anche *pattern - il ***inversoon of control***
- voce di Wikipedia (en) [qui](https://en.wikipedia.org/wiki/Inversion_of_control).
- In Vogella *dependency injection*, appunto un *inversion of control* [qui](https://www.vogella.com/tutorials/SpringDependencyInjection/article.html).

Un esempio del cambio di filosofia (tratto da Stack Overflow)
```java
public class TextEditor {

    private SpellChecker checker;

    public TextEditor() {
        this.checker = new SpellChecker();
    }
}
```
invertendo la dipendenza
```java
public class TextEditor {

    private IocSpellChecker checker;

    public TextEditor(IocSpellChecker checker) {
        this.checker = checker;
    }
}

...

SpellChecker sc = new SpellChecker(); // dependency
TextEditor textEditor = new TextEditor(sc);
```
ora passiamo ad usare le *dependency injection* in Spring

## `@Service`

Usiamo questa *annotation* per definire classi, dei ***component*** - vedi annotation `@Component`, [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/stereotype/Component.html) per le API - 
che offrono una qualche *business functionality* ovvero che contribuiscono a definire la *business logic*;
per le API vedi [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/stereotype/Service.html) anche se non vi è molto.

## `@Autowired`

Siamo all'altro versante dell'*injection*, [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/beans/factory/annotation/Autowired.html) per le API.

## `@Resource`

Un'alternativa ad `@Autowired` ... meglio a mio giudizio quest'ultima in quanto `@Resource` non è Spring ... ma lasciamo come in originale. Vediamo un esempio, ancora preso da StackO verflow
```java
interface parent {}

@Service("actualService")
class ActualService implements parent{}

@Service("stubbedService")
class SubbedService implements parent{}
```
abbiamo già visto che i *service* sono *component*, usando `@Autowired` possiamo scrivere
```java
@Autowired
@Qualifier("actualService")
Parent object;
```
togliendo ogni ambiguità! Mentre con `@resource` possiamo scrivere
```java
@Resource(name="${service.name}")
Parent object;  
```
settando in `application.properrties`
```
#service.name=actualService
 service.name=stubbedService
```

### redirect

Per comprendere per quale motivo usiamo la *redirection* in
```java
 @PostMapping("/")
String uploadFile(RedirectAttributes ra, @RequestParam(value = "myFile") MultipartFile multipartFile) throws IOException {
    try{
        service.save(multipartFile);
        ra.addFlashAttribute("message", "uploaded file" + multipartFile.getOriginalFilename());
        return "redirect:/";
    } catch (RuntimeException e) {
        ra.addFlashAttribute("message", "no file");
        return "redirect:/";
    }
}
```
vediamo in alternativa cosa avremmo potuto fare!
```java
@PostMapping("/")
String uploadFile(Model model, @RequestParam(value = "myFile") MultipartFile multipartFile) throws IOException {
    try{
        service.save(multipartFile);
        model.addAttribute("message", "uploaded file" + multipartFile.getOriginalFilename());
        model.addAttribute("files", service.loadAll().map(
                path -> new FileInfo(path.getFileName().toString(), "localhost:8080/files/" + path.getFileName().toString()))
                .collect(Collectors.toList()));
        return "uploadForm";
    } catch (RuntimeException e) {
        model.addAttribute("message", "no file");
        model.addAttribute("files", service.loadAll().map(
                path -> new FileInfo(path.getFileName().toString(), "localhost:8080/files/" + path.getFileName().toString()))
                .collect(Collectors.toList()));
        return "uploadForm";
    }
}
```

## properties

Nel file `application.properties` avremmo potuto controllare le dimensioni massime e minime di caricamento
```
spring.servlet.multipart.max-file-size=128KB
spring.servlet.multipart.max-request-size=128KB
```
