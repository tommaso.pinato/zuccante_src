# am014_authentication_ouath2

L'esempio è tratto, con i soliti aggiustamenti, dall'esempio [qui](https://www.websparrow.org/spring/spring-boot-spring-security-with-jpa-authentication-and-mysql).

## creazione progetto

- Spring Web
- Spring Boot DevTools
- Security
- OUAuth2 Client
- Mustache

# preparazione

Entrare in **Google API Console** [qui](https://console.developers.google.com/) e creare (o riutilizzare) un nuovo progetto,
- **segno + in alto** creare credenziali
- Schermata consenso OAuth: questo è un progetto di **TEST** non vincolato ad utenti di una data organizzazione, tipo di utente **esterno**, aggiungiamo gli utenti di test
- creiamo le credenziali per un'applicazione web (sempre recuperabili in ogni momento) di cui **teniamo traccia**
- aggiungere fra **URL reindirizzamento autorizzari** `http://localhost:8080/login/oauth2/code/google`
- aggiungere

e quindi impostiamo (questa volta usiamo un file `yml` di configurazione)
```
spring:
  security:
    oauth2:
      client:
        registration:
          google:
            clientId: 730521211266-gh618so41phqi6mk34b3ar433gpb1c06.apps.googleusercontent.com
            clientSecret: Vowx_lpO04dCgoBuqjJ6WZCa

```

## Autenticazione

L'annotazione `@AuthenticationPrincipal` permette di recuperare in un argomento, [qui](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/core/annotation/AuthenticationPrincipal.html)
per la doc. `AuthenticationManager` ritorna `Authentication` da cui si può ricavare un *principal* come ad esempio il noto `UserDetails`, [qui](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/core/userdetails/UserDetails.html) per la documentazione.
In alternativa
```java
authentication.getPrincipal();
```
Implementando `OAuth2User` (vedi esempio successivo): l'argomento è un `OAuth2User`, [qui](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/oauth2/core/user/OAuth2User.html) per la documentazione, un'interfaccia che possiede i seguenti metodi
- `getName()`,
- `getAttribute(String attribute)`, 
- `getAttributes()` e
- `getAuthorities()`




## altri materiali

[1] "How to create Google OAuth Credentials (Client ID and Secret)" su YouTube da Code Java [qui](https://www.youtube.com/watch?v=xH6hAW3EqLk).  
[2] "Spring Boot and OAuth2" [qui](https://spring.io/guides/tutorials/spring-boot-oauth2/).  
[3] "Spring Security 5 – OAuth2 Login" su Baeldung [qui](https://www.baeldung.com/spring-security-5-oauth2-login).  