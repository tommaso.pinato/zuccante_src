import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.stream.*;

public class Es001 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        System.out.println(numbers);
        List<Integer> twoEvenSquares = numbers.stream() // convert to a stream
        .filter(n -> {
            System.out.println("filtering " + n); 
            return n % 2 == 0; 
        })
        .map(n -> {
            System.out.println("mapping " + n);
            return n * n;
        })
        .limit(2)
        .collect(Collectors.toList());
        System.out.println(twoEvenSquares);
    }
}

