import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Future04 {

    public static void main(String args[]) {

        ExecutorService executor = Executors.newFixedThreadPool(10);
        List<Future<String>> list = new ArrayList<Future<String>>();
        Callable<String> callable = new MyCallable();
        for (int i = 0; i < 100; i++) {
            Future<String> future = executor.submit(callable);
            list.add(future);
        }
        for (Future<String> fut : list) {
            try {
                LocalTime time = LocalTime.now();
                System.out.println(time + "::" + fut.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        executor.shutdown();
    }

}

class MyCallable implements Callable<String> {

    static Random rnd = new Random(); 

    @Override
    public String call() throws Exception {
        int delay = 1 + rnd.nextInt(2); 
        try {
            Thread.sleep(delay * 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Thread.currentThread().getName();
    }

}
