import java.util.concurrent.Future;

public class Future01 {

    public static void main(String[] args) throws Exception {

        SquareCalculator calc = new SquareCalculator();
        Future<Integer> fut = calc.calculate(4);

        while(!fut.isDone() ) {
            System.out.println("Calculating...");
            Thread.sleep(500);
        }
        
        Integer result = fut.get();
        System.out.println("result:" + result);
        
        System.out.println("shutdown");
        calc.shutdown();

    }
    
}