const Es005 = {
    data() {
        return {
            memos: [{text: "primo"}, {text: "secondo"}]
        }
    },
    methods: {
        addMemo(text) {
            if (text.length == 0) return;
            this.memos.push({ text: text })
        },
        deleteMemo(data) {
            console.log("delete: " + data);
            this.memos.splice(data, 1);
        },
        testLog(data) {
            console.log("delete: " + data);
        },
        deleteAll() {
            this.memos = []
        }
    }
}

const app = Vue.createApp(Es005);

app.component('memo', {
    props: ['index', 'text'],
    methods: {},
    template:
        `<button @click="$emit('deleteMemo', index)">delete</button> : {{ text }}`
});

app.mount('#es005');
