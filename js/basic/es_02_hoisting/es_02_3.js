console.log("hoisting demonstration using \"const\"\n")

const x = 1;

{
   const x = 2
   console.log(x)
}

console.log(x)