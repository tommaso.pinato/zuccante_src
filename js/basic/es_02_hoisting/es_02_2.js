console.log("hoisting demonstration using \"let\"\n")

let x = 1;

function foo() {
  console.log(x); // ReferenceError: x is not defined
  let x = 2;
  console.log(x);
}

foo();

console.log(x);