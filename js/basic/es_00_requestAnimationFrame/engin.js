let requestID;



function doSomething() {
  setTime();
  requestID = requestAnimationFrame(doSomething);
}

function setTime() {
  let d = new Date();
  let span = document.getElementById("animation");
  span.innerHTML = d.getTime();
}

function stopDoing() {
  cancelAnimationFrame(requestID);
}

requestAnimationFrame(doSomething); // comment
