# Node

Con **node** possiamo scrivere server web, [qui](https://nodejs.org/it/) la sua home page, esso si basa su **V8**, [qui](https://v8.dev/) l'home page, a sua volta scritto in *C++*.


## npm settings

L'installazione **global** dei moduli avviene nella directory apposita (vedasi la [documentazione](https://docs.npmjs.com/files/folders)).  Nei sistemi Linux vengono impostate di default in `/usr`. L'utente però necessità di una directory con i permessi (altrimenti nel progetto viene creata la directory di installazione globale che chiaramente non lo è più di fatto). Nel mio caso, dopo aver settato il prefix (come utente senza `sudo`, creata la cartella dando il possesso all'utente ed al suo gruppo)
```
npm config set prefix '/opt/npm_global'
```
(gli `'` non sono necessari in realtà), vedi anche [qui](https://docs.npmjs.com/cli/config). Per i folder usati da `npm` si può andare [qui](https://docs.npmjs.com/files/folders). Eventualmente
```
export PATH=~/opt/npm_global/bin:$PATH
```
in `.profile` e senza log-in e logout
```
source ~/.profile
```
Dando
```
npm config get prefix
```
ottengo
```
/opt/npm_global
```
Tale info l'avremmo potuta ottenere osservando il contenuto del file `.npmrc` la cache la troviamo
``` 
Vediamo alcuni usi di `npm config`
```
genji@thinkpad:~$ npm config -h
npm config set <key> <value>
npm config get [<key>]
npm config delete <key>
npm config list
npm config edit
npm set <key> <value>
npm get [<key>]

alias: c
```
Inoltre alcuni moduli danno a disposizione eseguibili 
```
genji@thinkpad:~$ npm bin
/home/genji/node_modules/.bin
```
Nella cartella dei moduli installati globalmente abbiamo 
```
bin  lib  share
```
in `bin` gli eseguibili cui ci si puà collegare da una cartella del `PATH`, ad esempio `bin` in `HOME`. Per vedere i moduli installati globalmente
```
genji@TPL530:/opt/npm_global$ npm list -g --depth 0
/opt/npm_global/lib
├── @angular/cli@7.3.8
├── http-server@0.11.1
├── nativescript@5.3.1
├── npm@6.9.0
└── typescript@3.4.3
```
Comando che potrebbe risultare utile
```
npm uninstall -g <package_name>
```
per aggiornare
```
npm update -g
```

## projects

Con
```
npm init
```
creiamo all'interno dell'applicazione il file json impostando
```
entry point: (index.js)
```
Ora diamo (senza opzione `--save` non più necessaria da npm 5)
```
npm install express
```
che salva il modulo in locale e modifica il file `package.json`; basterà poi usare
```
npm install 
```
per installare i moduli specificati in `package.json` (ad esempio dopo averli caricati con git).

## git

Se si vuole evitare la fatica di crearsi il proprio `.gitignore` si può procedere cercando in rete, [qui](https://github.com/github/gitignore/blob/master/Node.gitignore) un esempio.