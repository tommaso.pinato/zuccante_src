# view e non solo

In questo esempio introduciamo i template ed altri dettagli. Qui sotto i moduli usati
-`koa`
- `koa-routes`
- `koa-views`
- `ejs`
Il modulo `koa-views` è uno dei più apprezzati, [qui](https://www.npmjs.com/package/koa-views), la sua home; è richiesta anche l'installazione del *template* che si andrà ad usare.

## i middleware

Attenzione all'ordine dei *middleware*! Porre `app.use(views ...)` in testa
``` javascript
app
  .use(views(__dirname + '/views', { extension: 'ejs' }))
  .use(router.routes());
```

## il template 

Usiamo **ejs** [qui](https://ejs.co/) un po' vintage ma leggibile anche dai *legacy*
``` html
<!DOCTYPE html>
<html>
    <head>
        <title><%= title %></title>
    </head>
<body>
    <h1><%= header %></h1>
    <ul>
        <% drinks.forEach((drink) => { %>
            <li><b><%= drink.name%></b>: <%= drink.ingredients %>.</li>
        <%  }); %>
    </ul>
</body>
</html> 
```
