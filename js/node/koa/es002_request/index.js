const Koa = require('koa');
const app = new Koa();

// request type
app.use(async (ctx, next) => {
  const now = Date.now();
  await next();
  const method = ctx.request.method;
  const url = ctx.request.URL;
  console.log(`request method: ${method}, URL: ${url}`);
});

// response
app.use(async ctx => {
    ctx.body = 'request';
});

app.listen(3000);