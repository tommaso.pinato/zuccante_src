# request

In questo esempio testeremo i *method*, provare con
```
curl -X ACTION localhost:3000
```
dove `ACTION` può essere: `GET` (default), `POST`, `PUT` e `DELETE`.