const EventEmitter = require('events');

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();
myEmitter.on('event', () => {
  console.log('event occurred!');
});
myEmitter.on('event', () => {
  console.log('event occurred again!');
});
myEmitter.on('event', () => {
  console.log('event occurred again and again!');
});

myEmitter.emit('event');