// USE: dart --enable-asserts es001_jsonDecode.dart

import 'dart:convert';

main(List<String> args) {
  var json1 = '[{"val": 40},{"val": 50}]';
  var json2 = '{"val": 60}';

  var decode1 = jsonDecode(json1);
  var decode2 = jsonDecode(json2);

  print("*** checking runtime type ***");
  if (decode1 is List<Map<String, dynamic>>) {
    print("decode1 is List<Map<STring, dynamic>>");
  }
  if (decode1 is List<dynamic>) {
    print("decode1 is List<dynamic>");
  }

  if (decode2 is Map<String, dynamic>) {
    print("decode2 is Map<String, dynamic>");
  }

  var val = decode1[0];
  print(val);
  print(val is Map<String, dynamic>);

  decode1 = decode1.cast<Map<String, dynamic>>();
  // decode1 = decode1 as List<Map<String, dynamic>>;
  if (decode1 is List<Map<String, dynamic>>) {
    print("decode1 is List<Map<STring, dynamic>>");
  }
}
