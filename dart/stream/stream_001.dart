import 'dart:async';

void main() { 
  Stream<int> stream = timedCounter(Duration(seconds: 1), 10);
  stream.listen((data)  => print('yeld: $data'));
  /* you need async for main!
  await for (var data in stream) {
    print('yeld: $data');
  }
  */
}

Stream<int> timedCounter(Duration interval, [int maxCount]) async* {
  int i = 0;
  while (true) {
    await Future.delayed(interval);
    yield i++;
    if (i == maxCount) break;
  }
}