import 'dart:async';

StreamController<String> streamController = StreamController.broadcast();

main() {
  print("Creating a StreamController...");

  streamController.stream.listen((data) {
    print("DataReceived1: $data");
  }, onDone: () {
    print("Task Done1");
  }, onError: (error) {
    print("Some Error1");
  });

  streamController.stream.listen((data) {
    print("DataReceived2: $data");
  }, onDone: () {
    print("Task Done2");
  }, onError: (error) {
    print("Some Error2");
  });

  // generate stream
  streamController.add("data 1");
  print("ciao");
  streamController.add("data 2");

  print("code controller is here");
}
