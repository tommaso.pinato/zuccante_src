import 'dart:async';
import 'dart:math';

// we need a Future ....
Future<int> getRandomNumber() {
  Random random = new Random();
  int n = random.nextInt(100);
  print("$n is generated");
  int t = 1 + random.nextInt(4);
  return Future.delayed(Duration(seconds: t), () => n );
}

void findSmallestNumberInList(List<int> lst) {
  print("all numbers are in:");
  lst.forEach((n) => print(n));
  lst.sort();
  int largest = lst.first;
  print("The smallest random int we generated was: ${largest}");
}

void main() async {
  Future.wait([getRandomNumber(), getRandomNumber(), getRandomNumber(), getRandomNumber()])
    .then((List<int> results) => findSmallestNumberInList(results));
}