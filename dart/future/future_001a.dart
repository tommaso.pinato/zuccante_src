import 'dart:async';

void main() async {
  // try to remove async
  int x = await number(5); // we need async
  print(x);
  print('after received');
}

Future<int> number(int n) {
  // we build a Future
  return Future.delayed(Duration(seconds: 5), () => n);
}
