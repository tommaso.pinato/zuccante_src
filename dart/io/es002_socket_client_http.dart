import 'dart:io';

// start using node: http-server -p 3000

void main() {
  
  String indexRequest = 'GET / HTTP/1.1\nConnection: close\n\n';

  Socket.connect("localhost", 3000).then((socket) {
    print('*** Connected to: '
      '${socket.remoteAddress.address}:${socket.remotePort}');
   
    socket.listen((List<int> data) { // Uint8List
      print(String.fromCharCodes(data).trim());
    },
    onDone: () {
      print("*** Done");
      socket.destroy();
    });
  
    //Send the request
    socket.write(indexRequest);
  });
}