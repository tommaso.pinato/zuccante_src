import 'dart:io';

main() {
  HttpServer
  .bind(InternetAddress.anyIPv6, 8080).then((server) {
    print('server is running');
    server.listen((HttpRequest request) {
        request.response.write('Hello, world!');
        request.response.close();
        });
    });
}