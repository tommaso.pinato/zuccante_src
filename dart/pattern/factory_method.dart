// an interface is a class, no abstract methods
class Animal {
  String getCall() => "booooooooooo";
}

class Dog implements Animal {
  String name;

  Dog(this.name);

  @override
  String getCall() => "Bau";
}

class Cat implements Animal {
  String name;

  Cat(this.name);

  @override
  String getCall() => "Miao";
}

enum Animals { felix, silvestro, boby, rudolf }

class AnimalFactory {
  static Animal createAnimal(Animals a) {
    Animal animal;
    switch (a) {
      case Animals.felix:
        animal = Cat("felix");
        break;
      case Animals.silvestro:
        animal = Cat("silvestro");
        break;
      case Animals.boby:
        animal = Dog("boby");
        break;
      case Animals.rudolf:
        animal = Dog("rudolf");
        break;
      default:
        animal = Animal();
        break;
    }
    return animal;
  }
}

void main() {
  Animal dog = AnimalFactory.createAnimal(Animals.boby);
  if (dog is Dog) print("boby is a dog");
}
