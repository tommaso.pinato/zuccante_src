class Notification {
  String message = "";
  DateTime timestamp = DateTime(1971);

  // initializer
  Notification(this.message, this.timestamp);
  // named constructor
  Notification.forNow(this.message) {
    timestamp = DateTime.now();
  }
}

class Observable {
  List<Observer> _observers = <Observer>[]; // see subscribe
  Observable([List<Observer> observers = const <Observer>[]]) {
    _observers = observers;
  }

  void registerObserver(Observer observer) {
    _observers.add(observer);
  }

  void notify_observers(Notification notification) {
    for (var observer in _observers) {
      observer.notify(notification);
    }
  }
}

class Observer {
  String name;

  Observer(this.name);

  void notify(Notification notification) {
    print(
        "[${notification.timestamp.toIso8601String()}] Hey $name, ${notification.message}!");
  }
}

class BarTender extends Observable {
  BarTender([List<Observer> observers = const <Observer>[]]) : super(observers);
  void spill() {
    print("...");
    notify_observers(Notification.forNow("a beer for you!"));
  }
}

void main() {
  var me = Observer("Andrea");
  var momy = BarTender(List.from([me]));
  var myFriend = Observer("Teo");
  momy.registerObserver(myFriend);
  momy.spill();
}
