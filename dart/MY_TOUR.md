# My Dart Tour

... for Java programmers. Ci sono già ottimi tutorial, ad esempio [questo](https://codelabs.developers.google.com/codelabs/from-java-to-dart/#0). Qui propongo alcuni appunti mal scritti ed incompleti, lo dichiaro subito, per i miei studenti.

## Const

Con `const` ci riferiamo **compiled time** constant, ovvero varibili (e non solo) fissate in fase di compilazione
```
const DateTime now = DateTime.now(); // ERR
```  
abbiamo qualcosa di simile `the construttor is not a const constructor`
```
List<int> list = const [1, 2, 3];
list [0] = 2; // ERR
``` 
come ci spuò aspettare!

# Set

Sono insiemi di dati dello stesso tipo
```
var set1 = <Object>{};
var set2 = <dynamic>{};
var set3 = <int>{};
var set = {}; // A MAP
```
Le prime due inizializzazioni sono analoghe e permettono di creare un set a tipi misti, ma di tipo `Object`, mentre l'ultima inizializzazione è farlocca e non permette di creare un `Set`.