import sys, pygame
pygame.init()

# the clock and frame
fpsClock = pygame.time.Clock()
FPS = 60

# the screen (size and color)
size = width, height = 500, 400
black = 0, 0, 0
screen = pygame.display.set_mode(size)

# a Surface for ball
ball = pygame.image.load("intro_ball.gif")
ballrect = ball.get_rect()
# ball speed
speed = [2, 2]

while 1:
    # get event from queue
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()

    # move ball
    ballrect = ballrect.move(speed)
    if ballrect.left < 0 or ballrect.right > width:
        speed[0] = -speed[0]
    if ballrect.top < 0 or ballrect.bottom > height:
        speed[1] = -speed[1]

    # paint the screen
    screen.fill(black)
    # draw ball into ballrect
    screen.blit(ball, ballrect)
    # update the screen
    pygame.display.update()
    # tick (wait ...)
    fpsClock.tick(FPS)
