import java.time.YearMonth;

public abstract class Shape implements Drawable {

    private float x = 0.0f;
    private float y = 0.0f;
    private float angle = 0.0f;

    @Override
    public abstract void draw();

    public abstract float getArea();

    public final void move(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public final void rotate(float rotation) {
        this.angle += rotation;
    }

    public final float getX() {
        return x;
    }

    public final float getY() {
        return y;
    }

    public float getAngle() {
        return angle;
    }
}