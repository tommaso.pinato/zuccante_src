public interface GraphicObject {
   void draw();
}

// functional interface