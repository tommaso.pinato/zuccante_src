public class InheritanceTest05 {

    public static void main(String[] args) {

        SuperClass obj1 = new SubClass();
        SubClass obj2 = new SubClass();
        System.out.println(obj1.staticMethod());
        System.out.println(obj2.staticMethod());
        System.out.println(((SubClass)obj1).staticMethod());
        System.out.println(SuperClass.staticMethod());
        System.out.println(SubClass.staticMethod());
    }

}

class SuperClass {

    public static String staticMethod() {
        return "inside super";
    }

}

class SubClass extends SuperClass {

    public static String staticMethod() {
        return "inside sub";
    }

}