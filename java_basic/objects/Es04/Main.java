public class Main {
    
    public static void main(String[] args) {
        
        MyArray my = MyArray.getRandomArray(11);
        System.out.println(my);
        System.out.println("insertion sort:");
        my.insertionSort();
        System.out.println("=================================");
        my = new MyArray(7);
        my.array[0] = 1;
        my.array[1] = 3;
        my.array[2] = 5;
        my.array[3] = 7;
        my.array[4] = 2;
        my.array[5] = 4;
        my.array[6] = 6;
        System.out.println(my);
        System.out.println("merge...");
        my.merge(0, 4, 6);
        System.out.println(my);
        System.out.println("=================================");
        my = MyArray.getRandomArray(11);
        System.out.println(my);
        my.mergeSort();  
    }
}
